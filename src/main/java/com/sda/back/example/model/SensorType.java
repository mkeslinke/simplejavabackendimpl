package com.sda.back.example.model;

public enum SensorType {
    SMOKE_DETECTOR,
    MOVEMENT_DETECTOR,
    OPEN_WINDOW_DETECTOR
}
