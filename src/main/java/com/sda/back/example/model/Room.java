package com.sda.back.example.model;

import java.util.List;

public class Room {
    private long roomId;
    private long houseId;
    private String roomName;

    public Room(long roomId) {
    }


    public Room(long roomId, String roomName) {
        this.roomId = roomId;
        this.roomName = roomName;
    }

    public Room(long roomId, long houseId, String roomName) {
        this.roomId = roomId;
        this.houseId = houseId;
        this.roomName = roomName;
    }

    @Override
    public String toString() {
        return "Room{" +
                "roomId=" + roomId +
                ", roomName='" + roomName +
                '}';
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public long getHouseId() {
        return houseId;
    }

    public void setHouseId(long houseId) {
        this.houseId = houseId;
    }
}
