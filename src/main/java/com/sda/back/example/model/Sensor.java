package com.sda.back.example.model;

public class Sensor {
    private long sensorId;
    private long roomId;

    private String sensorName;
    private SensorType sensorType;
    private boolean triggered;
    private double value;

    public Sensor(long sensorId, String sensorName, SensorType sensorType, boolean triggered) {
        this.sensorId = sensorId;
        this.sensorName = sensorName;
        this.sensorType = sensorType;
        this.triggered = triggered;
    }

    public Sensor(long sensorId, String sensorName, SensorType sensorType, long roomId) {
        this.sensorId = sensorId;
        this.sensorName = sensorName;
        this.sensorType = sensorType;
        this.roomId = roomId;
    }

    public Sensor(long sensorId, String sensorName, SensorType sensorType, double value) {
        this.sensorId = sensorId;
        this.sensorName = sensorName;
        this.sensorType = sensorType;
        this.value = value;
    }

    public long getSensorId() {
        return sensorId;
    }

    public void setSensorId(long setId) {
        this.sensorId = sensorId;
    }

    public void setTriggered(boolean triggered) {
        this.triggered = triggered;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getSensorName() {
        return sensorName;
    }

    public void setSensorName(String sensorName) {
        this.sensorName = sensorName;
    }

    public SensorType getSensorType() {
        return sensorType;
    }

    public void setSensorType(SensorType sensorType) {
        this.sensorType = sensorType;
    }

    public boolean isTriggered(){
        return triggered;
    }
}
