package com.sda.back.example.model;

import java.util.HashMap;
import java.util.List;

public class House {
    private long houseID;
    private String houseName;
    private long owner;

    public House() {
    }

    public House(long houseID, String houseName, long owner) {
        this.houseName = houseName;
        this.owner = owner;
        this.houseID = houseID;
    }

    public long getHouseID() {
        return houseID;
    }

    public void setHouseID(long houseID) {
        this.houseID = houseID;
    }

    public String getHouseName() {
        return houseName;
    }

    public void setHouseName(String houseName) {
        this.houseName = houseName;
    }

    public long getOwner() {
        return owner;
    }

    public void setOwner(long owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "House{" +
                "houseID=" + houseID +
                ", houseName='" + houseName + '\'' +
                ", owner=" + owner +
                '}';
    }
}
