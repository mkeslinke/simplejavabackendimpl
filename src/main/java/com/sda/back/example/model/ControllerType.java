package com.sda.back.example.model;

public enum ControllerType {
    THERMOSTAT,
    SWITCHER,
    ALARM,
    LOCK
}
