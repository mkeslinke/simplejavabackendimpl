package com.sda.back.example.model;

public class Controller {
    private long controllerId;
    private String controllerName;
    private long roomId;
    private ControllerType controllerType;

    public String getControllerName() {
        return controllerName;
    }

    public Controller(long controllerId, String controllerName, ControllerType controllerType, long roomId) {
        this.controllerName = controllerName;
        this.controllerType = controllerType;
        this.controllerId = controllerId;
        this.roomId = roomId;
    }

    public long getControllerId() {
        return controllerId;
    }

    public void setControllerId(long controllerId) {
        this.controllerId = controllerId;
    }

    public void setControllerName(String controllerName) {
        this.controllerName = controllerName;
    }

    public ControllerType getControllerType() {
        return controllerType;
    }

    public void setControllerType(ControllerType controllerType) {
        this.controllerType = controllerType;
    }
}
