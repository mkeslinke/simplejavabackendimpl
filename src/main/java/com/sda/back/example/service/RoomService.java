package com.sda.back.example.service;

import com.sda.back.example.model.ControllerType;
import com.sda.back.example.model.Room;
import com.sda.back.example.model.SensorType;

public interface RoomService {


    boolean addRoom( String roomName);
    boolean removeRoom(String roomName);

}
