package com.sda.back.example.service;

import com.sda.back.example.model.Controller;
import com.sda.back.example.model.ControllerType;
import com.sda.back.example.model.House;
import com.sda.back.example.model.Room;

import java.util.LinkedList;

public class ControllerServiceImpl implements ControllerService{

    private static long CONTROLLER_ID_COUNTER = 1;
    private LinkedList<Controller> controllerList = new LinkedList<>();

    @Override
    public void addController(String controllerName, long roomId, ControllerType controllerType) {
        controllerList.add(new Controller(CONTROLLER_ID_COUNTER++, controllerName, controllerType, roomId));
    }



    @Override
    public void removeController(String controllerName) {
        controllerList.remove(controllerName);
    }

    @Override
    public void turnOn(long controllerId) {

    }

    @Override
    public void turnOff(long controllerId) {

    }

    @Override
    public void setToValue(long controllerId, int value) {

    }
}
