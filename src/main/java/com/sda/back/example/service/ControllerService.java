package com.sda.back.example.service;

import com.sda.back.example.model.Controller;
import com.sda.back.example.model.ControllerType;
import com.sda.back.example.model.Room;

public interface ControllerService {
    void turnOn (long controllerId);
    void turnOff (long controllerId);
    void setToValue (long controllerId, int value);

    void addController(String controllerName, long roomId, ControllerType controllerType);
    void removeController (String controllerName);

}
