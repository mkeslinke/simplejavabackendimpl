package com.sda.back.example.service;


import com.sda.back.example.model.Room;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class RoomServiceImpl implements RoomService {

    private static long ROOM_ID_COUNTER = 1;
    private LinkedList<Room> rooms = new LinkedList<>();

    @Override
    public boolean addRoom(String roomName) {
        if (roomExist(roomName)) {
            return false;
        } else {
            rooms.add(new Room(ROOM_ID_COUNTER++, roomName));
            return true;
        }
    }

    public boolean roomExist(String roomName) {
        for (Room room : rooms) {
            if (room.getRoomName().equals(roomName)) {
                System.out.println("Pokój istnieje");
            }
        }
        return true;
    }


    @Override
    public boolean removeRoom(String roomName) {
        if (roomExist(roomName)) {
            for (Room room : rooms) {
                if (room.getRoomName().equals(roomName)) {
                    rooms.remove(room);
                    break;
                }
            }
            return true;
        } else {
            System.out.println("Such room do not exists!");
            return false;
        }


    }
}
