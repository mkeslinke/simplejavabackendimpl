package com.sda.back.example.service;

import com.sda.back.example.model.House;
import com.sda.back.example.model.Room;
import com.sda.back.example.model.Sensor;
import com.sda.back.example.model.SensorType;

public interface SensorService {

    boolean addSensor (SensorType sensorType, String sensorName, long roomId);
    boolean removeSensor (String sensorName);

boolean isPresent (long sensorId);
Sensor getSensorStatus (long sensorId);


}
