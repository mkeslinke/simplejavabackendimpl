package com.sda.back.example.service;

import com.sda.back.example.model.House;

import java.util.LinkedList;

public class HouseServiceImpl implements HouseService {

    private static long HOUSE_ID_COUNTER = 1;
    private LinkedList<House> houses = new LinkedList<>();

    protected HouseServiceImpl() {
    }

    @Override
    public boolean addHouse(String houseName, long owner) {
        if (houseExist(houseName)) {
            return false;
        } else {
            houses.add(new House(HOUSE_ID_COUNTER++, houseName, owner));
        }
        return true;
    }

    @Override
    public boolean houseExist(String houseName) {
        for (House house : houses) {
            if (house.getHouseName().equals(houseName)) {
                System.out.println("Such house already exists!");
                return true;
            }
        }
        return false;
    }

    @Override
    public House gethomeByName(String houseName) {
        for (House house : houses) {
            if (house.getHouseName().equals(houseName)) {           //// Odszukanie po nazwie domu (możliwość ściągnięcia ID
                return house;
            }
        }
        return null;
    }

    @Override
    public void removeHouse(String houseName) {
        if (houseExist(houseName)) {
            houses.remove(houseName);
            System.out.println("House " + houseName + " removed.");
        } else {
            System.out.println("Such house do not exists");
        }
    }

}

