package com.sda.back.example.service;

import com.sda.back.example.model.User;
import org.apache.commons.codec.digest.DigestUtils;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

public class UserServiceImpl implements UserService {

    private static long USER_ID_COUNTER = 1;
    //    private List<User> users = new ArrayList<>();
    private Map<Long, User> users = new HashMap<>(); // mapa id -> user

    protected UserServiceImpl(){
    }

    public User getUserWithId(long userId) {
        return users.get(userId);
    }

    public boolean registerUser(String login, String password) {
        // TODO: zweryfikować login użytkownika - czy jest unikalny
        // Jeśli użytkownik o danym loginie istnieje, to nie możemy go ponownie zarejestrować
        if (userExists(login)) {
            return false;
        }
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");

            String hashed = DigestUtils.md5Hex(password).toLowerCase();

            System.out.println("Encrypted password is: " + hashed);

            User newUser = new User(USER_ID_COUNTER++, login, hashed);

            users.put(newUser.getUserId(), newUser);
            return true;
        } catch (NoSuchAlgorithmException e) {
            System.err.println("Unable to encrypt password - no such algorithm.");
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public User getUsers(){
        System.out.println(users);
        return null;
    }
    @Override
    public boolean userExists(String login) {
        return false;
    }
}
