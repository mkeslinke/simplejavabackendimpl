package com.sda.back.example.service;

import com.sda.back.example.model.House;
import com.sda.back.example.model.Room;
import com.sda.back.example.model.User;

import java.util.LinkedList;


public interface HouseService {

    boolean addHouse(String houseName, long owner);
    boolean houseExist(String houseName);
    void removeHouse(String houseName);
    House gethomeByName(String houseName);

}
