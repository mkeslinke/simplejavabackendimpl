package com.sda.back.example.service;

import com.sda.back.example.model.Room;
import com.sda.back.example.model.Sensor;
import com.sda.back.example.model.SensorType;

import java.util.LinkedList;

public class SensorServiceImpl implements SensorService {

    private static long SENSOR_ID_COUNTER = 1;
    private LinkedList<Sensor> sensorsList = new LinkedList<>();

    @Override
    public boolean addSensor(SensorType sensorType, String sensorName, long roomId) {
        if (sensorExist(sensorName)) {
            return false;
        } else {
        sensorsList.add(new Sensor(SENSOR_ID_COUNTER++, sensorName, sensorType, roomId));
    }
    return true;
    }

        public boolean sensorExist(String sensorName) {
            for (Sensor sensor : sensorsList) {
                if (sensor.getSensorName().equals(sensorName)) {
                    System.out.println("Sensor already exists");
                }
            }
            return true;
        }


        @Override
    public boolean removeSensor(String sensorName) {
       if(sensorExist(sensorName)){
        sensorsList.remove(sensorName);
           System.out.println("Sensor " + sensorName + " removed");
    }
    else{
           System.out.println("Sensor " + sensorName + " was NOT removed");

       }return false;}

    protected SensorServiceImpl() {
    }


    @Override
    public boolean isPresent(long sensorId) {
        return false;
    }

    @Override
    public Sensor getSensorStatus(long sensorId) {
        return null;
    }


}
