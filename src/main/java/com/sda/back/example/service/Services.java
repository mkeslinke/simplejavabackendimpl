package com.sda.back.example.service;

public class Services {

    private static final Services INSTANCE = new Services();

    private final SensorService sensorService = new SensorServiceImpl();
    private final UserService userService = new UserServiceImpl();
    private final HouseService houseService = new HouseServiceImpl();

    public static Services getINSTANCE() {
        return INSTANCE;
    }

    public UserService getUserService() {
        return userService;
    }

    public HouseService getHouseService() {
        return houseService;
    }

    public SensorService getSensorService() {
        return sensorService;
    }
}
