package com.sda.back.example.views;

import com.sda.back.example.controller.HouseController;
import com.sda.back.example.controller.UserController;

public class CommandParser {
    private UserController userController = new UserController();
    private HouseController houseController = new HouseController();

    public void parseLine(String line) {
        String[] words = line.trim().split(" ");
        if (words[0].toLowerCase().equals("user")) {
            parseUserCommand(words);
        }
        if (words[0].toLowerCase().equals("addhouse")) {
            parseHouseCommand(words);
        }
        if (words[0].toLowerCase().equals("removehouse")) {
            parseHouseCommandRemoveRoom(words);
        }
        if (words[0].toLowerCase().equals("houseinfo")) {
            parseHouseCommandGetInfo(words);
        }
    }

    private void parseHouseCommandGetInfo(String[] words) {
        if (words[1].equals("info")) {
            houseController.getHouseStatus(words[2]);
        }
    }

    private void parseHouseCommandRemoveRoom(String[] words) {
        System.out.println("usuwanie domu");
        if (words[1].equals("remove")) {
            houseController.removeHouse(words[2]);
        }
    }

    private void parseHouseCommand(String[] words) {
        System.out.println("dodanie domu");
        if (words[1].equals("add")) {
            houseController.addHouse(words[2], Long.parseLong(words[3]));
        }
    }

    private void parseUserCommand(String[] words) {
        if (words[1].equals("register")) {
            System.out.println("Rejestracja użytkownika");
            userController.registerUser(words[2], words[3]);
        } else if (words[1].equalsIgnoreCase("login")) {
            System.out.println("Dane o użytkowniku " + words[2]);
            userController.getUserInfo(Long.parseLong(words[2]));
        }else if (words[1].equals("all")){
            System.out.println("Dane o wszystkich uzytkownikach");
            userController.getUsers();
        }
    }
}


