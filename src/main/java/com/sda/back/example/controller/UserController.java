package com.sda.back.example.controller;

import com.sda.back.example.model.User;
import com.sda.back.example.service.Services;
import com.sda.back.example.service.UserService;
import com.sda.back.example.service.UserServiceImpl;

public class UserController {

    private UserService userService = Services.getINSTANCE().getUserService();

    // Odpowiada za odpowiednie wywołanie zadania na serwisie, oraz wyświetlenie odpowiedzi

    public void registerUser(String login, String password){
        if(userService.registerUser(login, password)){
            System.out.println("Udało Ci się zarejestrować, Witamy!");
        }else{
            System.err.println("Nie udało się zarejestrować :(");
        }
    }

    public void getUsers(){
        User u = userService.getUsers();
        if (u !=null){
            System.out.println("Users info: " + u);
        }
    }

    public void getUserInfo(long userid){
        User u = userService.getUserWithId(userid);
        if (u != null){
            System.out.println("Users info: ");
            System.out.println(u);
        }
        else {
            System.out.println("Unable to print user info");
        }
    }

    //TODO: login

}
