package com.sda.back.example.controller;

import com.sda.back.example.model.House;
import com.sda.back.example.model.Room;
import com.sda.back.example.model.User;
import com.sda.back.example.service.HouseService;
import com.sda.back.example.service.Services;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class HouseController {
    private HouseService houseService = Services.getINSTANCE().getHouseService();

    public void addHouse(String houseName, long owner) {
        if (houseService.addHouse(houseName, owner)) {
            System.out.println("House added!");
        } else {
            System.out.println("Unnable to add house!");
        }
    }

    public boolean houseExist(String houseName){
            houseService.houseExist(houseName);
            return true;
    }

    public void getHouseStatus(String houseName){
        System.out.println(houseService.gethomeByName(houseName));
    }

    public void removeHouse(String houseName){
        houseService.removeHouse(houseName);
    }

}
